import { IProps } from './types';
declare function Button(props: IProps): import("react/jsx-runtime").JSX.Element;
export default Button;
