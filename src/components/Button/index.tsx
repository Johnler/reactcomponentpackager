import React from 'react';
import { IProps } from './types';

function Button(props: IProps) {
    const { label } = props;
    return (
        <input type="submit" value={label}/>
    );
}

export default Button;