import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from './components'

function App() {
  return (
    <div className="App">
        <Button label='Hello'/>
    </div>
  );
}

export default App;
